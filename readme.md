**Team**

name: JKU-Beta

members: Sinan Arnaut, Franz Berger, Martin Schobesberger, Stefan Wolfsteiner

**Introduction**

This is the final code used for the creative track in the Spotify RecSys Challenge. 
It only contains code this is eventually used to receive 10% precision (based on the 50% evaluation schema), but none of the other approaches that we have tried.
This file does not contain detailed information about all functions; for the predictor and functions used by the predictor please see
comments directly in the code.

Due to different completeness of the database (i. e. insert errors on our or your side, it can happen that you will receive
slightly different results than we do; if the difference is significant, please open an issue!).

**Used packages**

To run everything, you must have the following packages installed on your system (not only the imports!):

-pymongo

-numpy 

-nltk (https://www.nltk.org/install.html)

-emoji

-json


**Main Idea**

We use a hybrid recommender that is highly based on the playist names. We first fetch tracks from playists that have the same name as the current
challenge playlist and then select tracks from this set based on their genre.

**MongoDB**

To speed up data access and minimize the storage, we decided to set up a MongoDB database.
To run the predictor you must have this as well. Inside MongoDB you can name the database and collections as you wish,
to do so see the section _config_. We also suggest using some database viewer like _Compass_.

**Spotify API**

For some of the insert functions (_insert_artist_info_spotify()_ fetches the genres from spotify) you will need the spotify api (https://developer.spotify.com/).
To you use it you must register as developer (free), create a project and request a token (https://developer.spotify.com/documentation/general/guides/authorization-guide/)

**Insert to MongoDB**

All database insert methods are inside the _db_insert.py_ file. These are needed to ensure correct access during prediction, 
their calls are in the main class. 

-store_playlist_in_mongoDB(): Opens each file in the mpd folder and stores each playlist in the trainingSet collection specified in the config file

-insert.insert_clean_name(): Creates a clean name for each playlist and stores it as additional field in the trainingSet collection

-insert.insert_top_tracks(): Inserts the top tracks (occurrence > 100) from the trainingSet collection in the trainingTracks collection specified in the config file

-insert.insert_artist_info_spotify(): Inserts all artists that occur in the trainingSet collection + their genres, fetched from spotify, into the trainingArtists collection specified in the config file

-insert.insert_track_info_spotify(): Stores the first genre from each artist from the trainingArtists collection in the corresponding track in the trainingTracks collection

-insert.insert_predictedName(challenge_set): For each challenge playlists that is not given a name (2000 playlists), predicts a name and stores it in the challengePlaylist collection specified in the config file
You can leave this out to save some time, but you will then predict this 2000 playlists randomly!

Please be aware that inserting all data can take a long time (approx. 20h when including the predictName function, a few hours otherwise)!

**Config**

The config.py file contains all important configurations that can be adjusted.
Please see the comments directly in the file to see which config does what.


**Predictor**

The _creativePredictor.py_ file is where the actual prediction takes place. The _predict()_ function is called for each challenge playlist.
It makes use of a hybrid recommendation and works as follows: 

1a. Fetch all playlists from the trainingSet collection that have the same name (directly or predicted, as the the current challenge playlist)

1b. From these playlists, fetch the top tracks ordered by their number of occurrences

2. If the number of this tracks is <= the number of tracks to predict for the current challenge playlist (num_tracks field in playlist), we also fetch playlists based on their
cleaned name. If the number of tracks is still too low, we return them directly ordered by their number of occurrences as it makes no sense to process these any further

3a. Else if there are tracks given in the challenge set, we calculate the genre distribution inside the challenge tracks (e. g. 90% rock, 10% metal) and predict songs from 
the retrieved trainings tracks w. r. t. this distribution; if this does not produce enough tracks, fill it up with tracks from the name prediction until 
num_tracks tracks are reached

4a. Else if there are no challenge tracks given we return them directly ordered by their number of occurrences

5. If less then 500 have been predicted so far, we fill the remaining 500 - numTracks tracks from playists with the most followers

**Creative**

The runCreative.py file contains every function call needed to run the prediction. You can split the calls into
different stages, however the order of the calls is important and must be in the exact order as specified 
in the runCreative.py file. Once you ran all the import functions, you can exclude them from the runCreative file and only run the
read_challenge function and the predictor.

**License**

Copyright 2018 Sinan Arnaut, Franz Berger, Martin Schobesberger, Stefan Wolfsteiner
               

      
      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at
      
          http://www.apache.org/licenses/LICENSE-2.0
      
      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.