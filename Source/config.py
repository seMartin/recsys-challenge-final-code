#These hold the path to the unzipped mpd directory, the challenge file and the submission file that will be created

paths = {
'mpd_path': '',
'challenge_file': 'challenge_set.json',
'submission_file': 'submission.csv'
}

#mongo db configurations
host='localhost'
port=27017

#name of the MongoDB database
databaseName = "recsys"

#name of the training set, this will hold all 1 million playlists
trainingSet = "trainingSet"

#name of the challenge playlists, this will hold playlists from the challenge file, but only those that do not have a name
challengePlaylists = "challengePlaylists"

#name of the training tracks, this will hold all tracks from the training set that occurr more than 100 times + their genre
trainingTracks = "trainingTracks"

#name of the training artists, this will hold all artists from the training track + the genres they produce
trainingArtists = "trainingArtists"
# spotify token, reqeust new token if invalid
spotify_token = ""

#spotify headers, you can leave this as it is
spotify_headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Bearer '+spotify_token,
}

#spotify parameters, you can leave this as it is, except you wish to fetch information from different markets
spotify_parameters = (
    ('market', 'US'),
)