import pymongo
import numpy as np
import re
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from emoji.unicode_codes import UNICODE_EMOJI
from nltk.corpus import wordnet
import math
import config as cfg

#selects 1500 tracks from the playlists with the most followers from the trainingSet collection
def buildRandomPlaylist(challengePlaylist, db):
    print("building random playlist")
    stop = False
    lastPlaylist = []
    for all in db[cfg.trainingSet].find().sort("num_followers", pymongo.DESCENDING).limit(250):
        if stop:
            break
        for all_tracks in all['tracks']:
            #print(all_tracks)
            if all_tracks['track_uri'] not in lastPlaylist and all_tracks['track_uri'] not in challengePlaylist[0]['tracks']:
                lastPlaylist.append(all_tracks['track_uri'])
                if len(lastPlaylist) >=1500:
                    stop = True
                    break
    print("random playlist built with "+str(len(lastPlaylist))+" tracks")
    return lastPlaylist

#selects tracks from the trainingSet collection from certain playlists and orders them by their number of occurrences
def trackPopularityByName(db, playlist_name, playlist_id="", nameType="name"):
    pipeline = [
        {"$match": {"$and": [{nameType: playlist_name}, {"pid": {"$ne": playlist_id}}]}},
        {"$unwind": "$tracks"},
        {"$group": {"_id": {"track_uri": "$tracks.track_uri", "track_name": "$tracks.track_name",
                            "artist_name": "$tracks.artist_name"}, "count": {"$sum": 1}}},
        {"$sort": {"count": -1}},
        {"$limit": 300}
    ]
    tracks = []
    try:
        tracks = list(db[cfg.trainingSet].aggregate(pipeline, allowDiskUse=True))
    except:
        None
    return tracks


def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return decorate


@static_vars(stopwords=np.concatenate((np.array((stopwords.words('english'))), np.array(
    ["music", "musik", "playlist", "songs", "favorites", "favourite", "favorite"]))))

#cleans the playlist name, removes emojis and stop words, transforms upper case characters, performs stemming
#returns 4 results, only the clean_name is used however!
def clean_name(data):
    """
    returns clean_name as list, clean_name, synonyms (for each word in clean_name if available), year (if in name, empty ("") otherwise)
    turns emoticons into text, removes stop words,

    :param data: the orignal name
    :return: list_of_words,  clean_name, synonyms, year
    """
    emojis = ' '.join(re.sub(":", "", UNICODE_EMOJI[c]) for c in data if c in UNICODE_EMOJI)
    data_lc = data.casefold()  # to lowercase and ß->ss
    data_lc = re.sub("work out", "workout",
                     data_lc)  # there seem to be a lot of "work out playlist" - others are called "workout"
    data = re.sub("[^a-z ]", "",
                  data_lc)  # remove non alphabetical characters -> no numbers; also no chinese characters - TODO look for this
    number = re.sub("[^0-9]", "", data_lc)  # get numbers -> most likely year info
    year = number[-2:]  # only store last two digits since most of the names are something like this ("summer '17")
    if year is not "" and int(year) < 10:
        year = ""

    words = word_tokenize(data + emojis)
    words = [w for w in words if w not in clean_name.stopwords]

    ps = PorterStemmer()  # only use word stem -> ["game","gaming","gamed","games"] -> game
    stem_words = set([ps.stem(w) for w in words])  # remove duplicates by using a set
    synonyms = set([s[0].lemmas()[0].name() for s in [wordnet.synsets(w) for w in words] if len(s) > 0])
    if len(synonyms) == 0:
        for s in [wordnet.synsets(w) for w in stem_words]:
            if len(s) > 0:
                synonyms.add(s[0].lemmas()[0].name())

    if len(stem_words) == 0:
        name = number
    else:
        name = " ".join(stem_words)

    return stem_words, name, " ".join(synonyms), year

#creates a dictionary where the key is a trackId and the values is the number of counts in both sets
#takes two sets (playlists based on name and clean name) and merges them into one dict
def count_tracks(set1, set2):
    tracks = {}
    for t in set1:
        if t['_id']['track_uri'] not in tracks:
            tracks[t['_id']['track_uri']] = t['count']
        else:
            tracks[t['_id']['track_uri']] += t['count']
    for t in set2:
        if t['_id']['track_uri'] not in tracks:
            tracks[t['_id']['track_uri']] = t['count']
        else:
            tracks[t['_id']['track_uri']] += t['count']

    tracks_sorted = {}
    for t in sorted(tracks, key = tracks.get, reverse=True):
        tracks_sorted[t] = tracks[t]
    return tracks_sorted


#for each challenge track, finds its genre and calculates the overall genre distribution of the challenge tracks relative to the playlist length
#returns a dict where the key is the genre and the values is the corresponding percentage in the challenge playlist, sorted descending
def getGenreDistribution(challengeTracks, db):
    genres = {}
    fail = 0
    for t in challengeTracks:
        try:
            genre = db[cfg.trainingTracks].find({"track_uri": t['track_uri']})[0]['genre']
            if genre[0] not in genres:
                genres[genre[0]] = 1
            else:
                genres[genre[0]] += 1
        except:
            fail+=1
    for g in genres:
        genres[g] = math.floor(((genres[g]*100) / (len(challengeTracks)-fail)))

    gSorted = {}

    for s in sorted(genres, key=genres.get, reverse=True):
        gSorted[s] = genres[s]
    return gSorted


#predicts num_tracks tracks from given set of tracks w.r.t genre distribution in the challenge_tracks
#if for a genre not enough tracks can be predicted, the reminder is pushed to the next genre
def predictByGenre(genres, tracks, db, num_tracks, challenge_tracks):
    track_genres = {}
    prediction = []
    reminder = 0

    for t in tracks:
        try:
            res = db[cfg.trainingTracks].find({'track_uri':t})
            genre = res[0]['genre'][0]
            track_genres[t] = genre
        except:
            None

    track_genres_sorted = {}
    for s in tracks:
        try:
            track_genres_sorted[s] = track_genres[s]
        except:
            None

    for g in genres:
        i = 0
        g_cnt = genres[g] * num_tracks
        for t in track_genres:
            if track_genres[t] == g and i < g_cnt + reminder and t not in challenge_tracks:
                prediction.append(t)
                i+=1
        if i < g_cnt:
            reminder += g_cnt - i
        else:
            reminder -= (i-g_cnt)


    # print(len(prediction))
    return prediction


def normalize_name(name):
    name = name.lower()
    name = re.sub(r"[.,\/#!$%\^\*;:{}=\_`~()@]", ' ', name)
    name = re.sub(r'\s+', ' ', name).strip()
    return name

def lowerCaseName(name):
    return name.casefold().strip()

#calculates the similarity of two playists by calculating the number of tracks from playlist_a in playlist_b
#e.g. all tracks of a are in b -> 100% similarity
def playlistSimilarity(playlist_a, playlist_b):
    sim = len(set(playlist_a).intersection(set(playlist_b))) / len(playlist_a)
    return sim

def predictFromMostFrequentArtist_alt(playlist, db):
    from collections import Counter
    artist_uris = []
    for t in playlist["tracks"]:
        artist_uris.append(t["artist_uri"])
    a = list(Counter(artist_uris).keys())
    b = list(Counter(artist_uris).values())
    tracks = []
    for i in range(len(a)):
        if b[i] / len(b) >= 0.2:
            artist_tracks = list(db[cfg.databaseName][cfg.trainingTracks].find({"artist_uri": a[i]}).sort("count", pymongo.DESCENDING).limit(500))
            if len(artist_tracks) > 0:
                for t in artist_tracks:
                    tracks.append(t["track_uri"])

    return tracks

def predictFromMostFrequentAlbum_alt(playlist, db):

    from collections import Counter
    album_uris = []
    for t in playlist["tracks"]:
        album_uris.append(t["album_uri"])
    a = list(Counter(album_uris).keys())
    b = list(Counter(album_uris).values())
    # ind=np.lexsort((a,b))
    # sorted_album = [(a[i],b[i]) for i in ind]
    max_idx = b.index(max(b))
    tracks = []

    album = a[max_idx]
    for a in list(db[cfg.databaseName][cfg.trainingTracks].find({"album_uri": album}).sort("count", pymongo.DESCENDING)):
        artist_uri = a['artist_uri']
        for t in list(db[cfg.databaseName][cfg.trainingTracks].find({"artist_uri": artist_uri}).sort("count", pymongo.DESCENDING).limit(500)):
            tracks.append(t["track_uri"])

            if len(tracks) > 500:
                break

        if len(tracks) > 500:
                break

    return tracks

def predictFromMostFrequentAlbum_notartist(playlist, db):
    from collections import Counter
    album_uris = []
    for t in playlist["tracks"]:
        album_uris.append(t["album_uri"])
    a = list(Counter(album_uris).keys())
    b = list(Counter(album_uris).values())
    max_idx = b.index(max(b))
    tracks = []

    album = a[max_idx]

    for t in list(db[cfg.databaseName][cfg.trainingTracks].find({"album_uri": album}).sort("count", pymongo.DESCENDING).limit(500)):
        tracks.append(t["track_uri"])

    return tracks