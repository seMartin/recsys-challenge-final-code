def write(playlists, filepath):
    challenge_file = open(filepath,"w+")
    challenge_file.write('team_info,JKU-Beta,main,jkubeta.recsys@gmail.com\n\n')

    for playlist in playlists:
        line = str(playlist["pid"])
        for track in playlist["tracks"]:
            line = line + ", " + track
        challenge_file.write(line + "\n")

    challenge_file.close()

def create(filepath):
    challenge_file = open(filepath,"w+")
    challenge_file.write('team_info,main,JKU-Beta,jkubeta.recsys@gmail.com\n\n')
    
    challenge_file.close()

def write_single(playlist, filepath):
    challenge_file = open(filepath,"a+")

    line = str(playlist["pid"])

    for track in playlist["tracks"]:
        line = line + ", " + track
    challenge_file.write(line + "\n")

    challenge_file.close()