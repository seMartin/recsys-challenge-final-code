from creativePredictor import NameClusterSelectionPredictor
import readData as read
import config as cfg
import time
import writeSubmission as write
import db_insert as insert

challenge_set = read.read(cfg.paths['challenge_file'])

##########################
#Include for storing data#
##########################


'''
insert.store_playlist_in_mongoDB()
insert.insert_clean_name()
insert.insert_top_tracks()
insert.insert_artist_info_spotify()
insert.insert_track_info_spotify()
insert.insert_predictedName(challenge_set)
'''

###########
#Predictor#
###########

predictor = NameClusterSelectionPredictor()
predictions = []
i = 0
start = time.time()
for challengePlaylist in challenge_set:
    i+=1
    if i % 100 == 0:
        print("Predicted "+str(i)+" playlists")
    pr = predictor.predict(challengePlaylist, False)
    if len(pr) != 500:
        print("ERROR")
    predictions.append({"pid": challengePlaylist["pid"], "tracks": pr})
print("duration for predicting from : "+str(len(predictions))+" playlists "+str(time.time()-start))
write.write(predictions, cfg.paths['submission_file'])
