from pymongo import MongoClient
import pymongo
from config import host, port, trainingSet, databaseName
import numpy as np
import re
import util as u
import config as cfg

class MainPredictor:
    def __init__(self, logging=False, creative=False):
        self.db = pymongo.MongoClient(host, port)
        self.LOGGING = logging
        self.excluded = None

    def predict(self, challengePlaylist,  exclude_challenge_playlist=False):
        eval_tracks = []
        for t in challengePlaylist["tracks"]:
            eval_tracks.append(t["track_uri"])

        # ind=np.lexsort((a,b))
        # sorted_album = [(a[i],b[i]) for i in ind]
        predicted_playlist = []

        if len(challengePlaylist["tracks"]) > 0 and sameAlbumRatio(challengePlaylist) > 0.3:
            print("same album predictions")
            album_tracks = u.predictFromMostFrequentAlbum_notartist(challengePlaylist, self.db)
            predicted_playlist = self.mergePlaylistsTrack(eval_tracks, album_tracks)
            print("Current playlist length: " + str(len(predicted_playlist)))

            if len(predicted_playlist) > 0 and len(predicted_playlist) < 500:
                res = u.predictFromMostFrequentAlbum_alt(challengePlaylist, self.db)
                res = self.mergePlaylistsTrack(eval_tracks, res)
                res = self.mergePlaylistsTrack(predicted_playlist, res)
                
                for t in res:
                    if len(predicted_playlist) >= 500:
                        break
                    if t not in predicted_playlist:
                        predicted_playlist.append(t)

                print("Current playlist length: " + str(len(predicted_playlist)))

        if len(predicted_playlist) < 500 and len(challengePlaylist["tracks"]) > 0 and sameArtistRatio(challengePlaylist) > 0.3:
            print("same artist predictions")
            
            res = u.predictFromMostFrequentArtist_alt(challengePlaylist, self.db)
            res = self.mergePlaylistsTrack(eval_tracks, res)
            res = self.mergePlaylistsTrack(predicted_playlist, res)

            for t in res:
                if len(predicted_playlist) >= 500:
                    break
                if t not in predicted_playlist:
                    predicted_playlist.append(t)

            print("Current playlist length: " + str(len(predicted_playlist)))

        if exclude_challenge_playlist==True:
            self.excluded = challengePlaylist["_id"]

        if len(predicted_playlist) < 500 and "name" in challengePlaylist:
            # Name only prediction
            print("Playlist name prediction for: " + challengePlaylist["name"])
            res = self.predict_playlist_by_name(challengePlaylist["name"], self.db, challengePlaylist["tracks"])
            res = self.mergePlaylistsTrack(eval_tracks, res)
            res = self.mergePlaylistsTrack(predicted_playlist, res)
                
            for t in res:
                if len(predicted_playlist) >= 500:
                    break
                if t not in predicted_playlist:
                    predicted_playlist.append(t)
            
            print("Current playlist length: " + str(len(predicted_playlist)))

        if len(predicted_playlist) < 500 and len(challengePlaylist["tracks"]) > 0 and float(5) / len(challengePlaylist["tracks"])  > 0.2:
            # Tracks only prediction
            print("Strict Playlist track prediction for: " + str(len(challengePlaylist["tracks"])) + " tracks")
            res = self.predict_playlist_by_tracks(challengePlaylist, True, self.db)
            res = self.mergePlaylistsTrack(eval_tracks, res)
            res = self.mergePlaylistsTrack(predicted_playlist, res)
                
            for t in res:
                if len(predicted_playlist) >= 500:
                    break
                if t not in predicted_playlist:
                    predicted_playlist.append(t)

            print("Current playlist length: " + str(len(predicted_playlist)))

        if len(predicted_playlist) < 500 and len(challengePlaylist["tracks"]) > 0:
            # Tracks only prediction
            print("Non-strict playlist track prediction for: " + str(len(challengePlaylist["tracks"])) + " tracks")
            res = self.predict_playlist_by_tracks(challengePlaylist, False, self.db)
            res = self.mergePlaylistsTrack(eval_tracks, res)
            res = self.mergePlaylistsTrack(predicted_playlist, res)
                
            for t in res:
                if len(predicted_playlist) >= 500:
                    break
                if t not in predicted_playlist:
                    predicted_playlist.append(t)

            print("Current playlist length: " + str(len(predicted_playlist)))

        if len(predicted_playlist) < 500:
            print("Add popular tracks!")
            more = self.predict_playlist_by_name("popular", self.db, challengePlaylist["tracks"])

            for t in more:
                if len(predicted_playlist) >= 500:
                    break
                if t not in predicted_playlist:
                    predicted_playlist.append(t)

        print(len(predicted_playlist))
        return predicted_playlist

    def mergePlaylists(self, predict_playlist_tracks, sorted_tracks):
        mergedPlaylist = set()

        for t in sorted_tracks:
            if len(mergedPlaylist) >= 500:
                break

            # tracks of challenge file must not be included!
            if t.track not in predict_playlist_tracks:
                mergedPlaylist.add(t.track)
                #print("Added track with playlist sim: " + str(t.playlist_sim) + " and track sim " + str(t.track_sim))

        return mergedPlaylist

    def mergePlaylistsTrack(self, predict_playlist_tracks, sorted_tracks):
        mergedPlaylist = []

        for t in sorted_tracks:
            if len(mergedPlaylist) >= 500:
                break

            # tracks of challenge file must not be included!
            if t not in predict_playlist_tracks:
                mergedPlaylist.append(t)
                #print("Added track with playlist sim: " + str(t.playlist_sim) + " and track sim " + str(t.track_sim))

        return mergedPlaylist

    def getSortKey(self, match):
        return match.playlist_sim * 1000 + match.track_sim


    def get_additional_tracks(self, tracks, client, strict):
        eval_tracks = []
        
        query = []
        queryid = []

        for t in tracks:
            eval_tracks.append(t)
            if (strict == False and len(query) <= 20) or (strict == True and len(query) <= 5):
                trackQuery = {"tracks.track_uri": t}
                query.append(trackQuery)
                queryid.append(t)
            else:
                break

        if strict == True:
            query = {"tracks.track_uri": {"$all": queryid}}
        else:
            query = {"$or": query}

        if self.excluded != None:
            #queryMultiple = {"$and": [{"_id": {"$ne": self.excluded }}, queryMultiple]}
            query = {"$and": [{"_id": {"$ne": self.excluded }}, query]}

        res = client[databaseName][trainingSet].find(query).sort("num_followers", pymongo.DESCENDING).limit(100)
        
        return self.handleResult(res, eval_tracks)

    def handleResult(self, res, eval_tracks):
        tracks = {}
        sim_tracks = []

        eval_tracks_count = len(eval_tracks)

        for r in res:
            train_tracks = []
            for t in r['tracks']:
                train_tracks.append(t['track_uri'])
            
            same_count = len(np.intersect1d(eval_tracks, train_tracks))
            
            if eval_tracks_count == 0:
                sim = 0
            else:
                sim = float(same_count)/len(eval_tracks)

            minsim = 0.02

            if sim == 0 or sim > minsim:
                for t in train_tracks:
                    if t not in eval_tracks:
                        sim_tracks.append(Match(t, sim, 1))

        maxtrackcount = 0
        maxsim = 0
        print("Analyzing " + str(len(sim_tracks)) + " tracks!")
        for t in sim_tracks:
            if t.track not in tracks:
                tracks[t.track] = Match(t.track, sim, 1)
            else:
                track_count = tracks[t.track].track_sim
                tracks[t.track].track_sim = track_count + 1
                if tracks[t.track].playlist_sim < t.playlist_sim:
                    tracks[t.track].playlist_sim = t.playlist_sim

        for k,t in tracks.items():
            if maxtrackcount < t.track_sim:
                maxtrackcount = t.track_sim
            if maxsim < t.playlist_sim:
                maxsim = t.playlist_sim

        print("max track count is " + str(maxtrackcount) + "; max sim is "+ str(maxsim))
        return self.mergePlaylists(eval_tracks, sorted(tracks.values(), key=self.getSortKey, reverse=True))

    def predict_playlist_by_name(self, challengePlaylistName, client, train_track):    
        query = {"nomalized_name": u.normalize_name(challengePlaylistName)}

        if self.excluded != None:
            query = {"$and": [{"_id": {"$ne": self.excluded }}, query]}

        db = client[databaseName]
        res = db[trainingSet].find(query).sort("num_followers", pymongo.DESCENDING).limit(200)

        eval_tracks = []
        for t in train_track:
            eval_tracks.append(t["track_uri"])

        tracks = self.handleResult(res, eval_tracks)
        
        print("Found " + str(len(tracks)) + " tracks!")

        if len(tracks) < 500 and len(tracks) > 0:
            print("Found " + str(len(tracks)) + " songs. Searching for additional.")
            additionaltracks = []
            if len(eval_tracks) == 0:
                self.get_additional_tracks(tracks, client, False)
            else:
                self.get_additional_tracks(eval_tracks, client, False)

            for t in additionaltracks:
                if len(tracks) >= 500:
                    break
                tracks.add(t)

        return tracks


    def predict_playlist_by_tracks(self, eval_playlist, strict, client):
        eval_tracks = []

        for t in eval_playlist["tracks"]:
            eval_tracks.append(t["track_uri"])

        return self.get_additional_tracks(eval_tracks, client, strict)

class Match:
    def __init__(self, track, playlist_sim, track_sim):
        self.playlist_sim = playlist_sim
        self.track_sim = track_sim
        self.track = track

def sameAlbumRatio(playlist): 
    from collections import Counter
    album_uris = []
    for t in playlist["tracks"]:
        album_uris.append(t["album_uri"])
    a = list(Counter(album_uris).keys())
    b = list(Counter(album_uris).values())
    maxi = max(b)
    return float(maxi) / len(playlist["tracks"])

def sameArtistRatio(playlist):
    from collections import Counter
    artist_uris = []
    for t in playlist["tracks"]:
        artist_uris.append(t["artist_uri"])
    a = list(Counter(artist_uris).keys())
    b = list(Counter(artist_uris).values())
    maxi = max(b)
    return float(maxi) / len(playlist["tracks"])