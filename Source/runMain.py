import config as cfg
import readData as read
import writeSubmission as submission
from mainPredictor import MainPredictor
from pymongo import MongoClient
import db_insert as insert

##########################
#Include for storing data#
##########################


'''
insert.store_playlist_in_mongoDB()
insert.insert_clean_name()
'''

###########
#Predictor#
###########

challenge_set = read.read(cfg.paths['challenge_file'])
preditor = MainPredictor()
predictions=list()
i = 0
prev_playlist=list()

submission.create(cfg.paths['submission_file'])
for challengePlaylist in challenge_set:
    print(str(i + 1) + ". prediction") 

    predicted_playlist =  preditor.predict(challengePlaylist)

    if len(predicted_playlist) < 500:
        evaltracks = [obj["track_uri"] for obj in challengePlaylist["tracks"]]
        print("Found tracks: " + str(len(predicted_playlist)) + ". Adding songs of previous playlist.")
        for track in predicted_playlist:
            if len(predicted_playlist) >= 500:
                break

            if track not in evaltracks:
                predicted_playlist.append(track)

    if len(predicted_playlist) < 500:
        print("Still not enough tracks! Found tracks: " + str(len(predicted_playlist)))
        #raise Exception('Still not enough tracks!') -> hacky alternative
        predicted_playlist=prev_playlist
    else:
        prev_playlist=predicted_playlist

    prediction = {"pid": challengePlaylist["pid"], "tracks": predicted_playlist}
    predictions.append(prediction)

    submission.write_single(prediction, cfg.paths['submission_file'])
    i = i + 1

submission.write(predictions, cfg.paths['submission_file'] + ".sav")