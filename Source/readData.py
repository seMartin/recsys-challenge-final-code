import json
import datetime
import numpy as np

def read(file):
    start_time = datetime.datetime.now()
    challenge_file = open(file)
    challenge_str = challenge_file.read()
    challenge_data = json.loads(challenge_str)
    end_time = datetime.datetime.now()
    print("INFO: it took {0} seconds to load the challange data".format((end_time-start_time).total_seconds()))
    return challenge_data["playlists"]

def open_JSON_playlist(name):
    learn_file = open(name)
    learn_str = learn_file.read()
    learn_data = json.loads(learn_str)
    learn_playlists = np.array(learn_data['playlists'])
    return learn_playlists