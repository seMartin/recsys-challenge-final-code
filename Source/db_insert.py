import pymongo as pm
import config as cfg
import os
import readData as read
import pymongo as pm
import util as util
import requests
import re


#stores each playlist from the mpd inside the trainingSet collection specified in the config
def store_playlist_in_mongoDB():
    print("storing playlists in MongoDB")
    path=cfg.paths['mpd_path']
    client = pm.MongoClient(cfg.host, cfg.port)
    db = client[cfg.databaseName]
    filenames = os.listdir(path)
    i = 0
    for filename in sorted(filenames):
        if filename.startswith("mpd.slice.") and filename.endswith(".json"):
            fullpath = os.sep.join((path, filename))
            learn_playlists = read.open_JSON_playlist(fullpath)
            for p_l in learn_playlists:
                i+=1
                if i%100 == 0:
                    print("stored "+str(i)+" playlists")
                db[cfg.trainingSet].insert_one(p_l)
    print("stored playlists in mongoDb")

#inserts the clean name for each playlist in the trainingSet collection
#see the clean_name function in util
def insert_clean_name():
    print("inserting clean name")
    #https://stackoverflow.com/questions/47489836/convert-emoji-unicode-to-text-in-python?rq=1
    #https://pythonspot.com/nltk-stop-words/
    client = pm.MongoClient(cfg.host, cfg.port)
    db = client[cfg.databaseName][cfg.trainingSet]
    noCleanName = db.find()
    i=0
    max=noCleanName.count()
    for ncn in noCleanName:
        if i%10000 == 0:
            print("{0}/{1} entries updated".format(i,max))
        i += 1
        try:
            words, name, synonymStr, year = util.clean_name(ncn["name"])
            nn = util.normalize_name(ncn["name"])
            db.update({"_id": ncn["_id"]}, {"$set": {"clean_name": name}})
            db.update({"_id": ncn["_id"]}, {"$set": {"normalized_name": nn}})
            db.update({"_id": ncn["_id"]}, {"$set": {"synonyms": synonymStr}})
            #if year is not "":
            db.update({"_id": ncn["_id"]}, {"$set": {"year": year}})
        except KeyError:
            print("no name")

#adds the genre as a field to the trainingTracks collection taken from the trainingArtists collection
def insert_track_info_spotify():
    client = pm.MongoClient(cfg.host, cfg.port)
    db = client[cfg.databaseName]
    tracks = list(db[cfg.trainingTracks].find())
    print("Inserting genre for trainingTracks: found {0} tracks".format(len(tracks)))
    tCnt = 0
    ids = []
    # update info for all tracks
    for t in tracks:
        tCnt += 1
        if tCnt % 100 == 0:
            print("Inserted genre for "+str(tCnt)+" of "+str(len(tracks))+" tracks")
        try:
            genre = client[cfg.databaseName][cfg.trainingArtists].find({"artist_uri":t["artist_uri"]})[0]["genre"]
            client[cfg.databaseName][cfg.trainingTracks].update({"_id": t["_id"]}, {"$set": {"genre": genre}})
        except:
            continue

#inserts each artist from the trainingSet collection + genre into the trainingArtists collection specified in the config
#genres are fetched via spotify API, if you receive an error during fetching, the token has expired (after 1 hour), in this case
#request a new one, put it in the config and set the startingPoint to the output of the error message (tCnt)
def insert_artist_info_spotify():
    startingPoint = 0
    print("Inserting trainingArtists into MongoDB")
    client = pm.MongoClient(cfg.host, cfg.port)

    pipeline = [
        {"$group": {"_id": {"artist_uri":"$artist_uri","artist_name":"$artist_name"},
                    "count": {"$sum": 1}}}

    ]
    artists = list(client[cfg.databaseName][cfg.trainingTracks].aggregate(pipeline,allowDiskUse=True))
    tCnt = 0
    for a in artists:
        tCnt+=1
        if tCnt < startingPoint:
            continue
        #continue
        if tCnt % 100 == 0:
            print(str(tCnt)+" of "+str(len(artists))+" artists inserted")
        artist_uri = re.sub("spotify:artist:", "", a['_id']["artist_uri"])
        # print(artist_uri)
        try:
            response = requests.get('https://api.spotify.com/v1/artists/'+artist_uri, headers=cfg.spotify_headers, params=cfg.spotify_parameters).json()

            try:
                g = response["genres"]
                f = response["followers"]
                client[cfg.databaseName][cfg.trainingArtists].insert_one(
                    {
                        "artist_uri":a['_id']["artist_uri"],
                        "artist_name":a['_id']["artist_name"],
                        "genre":g,
                        "followers":f,
                        "count":a['count']
                    }
                )
            except KeyError:
                print(tCnt)
                break
        except requests.exceptions.ConnectionError:
            print("connection error")
            print(tCnt)

#inserts the tracks from the trainingSet collection into the trainingTracks collection specified in the config that occurr more than 100 times
def insert_top_tracks():
    print("Inserting top tracks into MongoDB")
    client = pm.MongoClient(cfg.host, cfg.port)
    db = client[cfg.databaseName][cfg.trainingSet]

    pipeline = [
        {"$unwind": "$tracks"},
        {"$group": {"_id": {"track_uri": "$tracks.track_uri", "track_name":"$tracks.track_name", "artist_name":"$tracks.artist_name","artist_uri":"$tracks.artist_uri","album_uri":"$tracks.album_uri"},
                    "count": {"$sum": 1},
                    'position_metric':{"$sum":{'$divide':[
                        {'$subtract':['$num_tracks','$tracks.pos']}
                        ,'$num_tracks']}
                    },
                    'popularity_metric':{'$sum':'$num_followers'}}},
        {"$match" : {
            "count": {"$gte": 100}
        }}
    ]

    tracks = list(db.aggregate(pipeline,allowDiskUse=True))
    tCnt = 0
    for t in tracks:
        tCnt+=1
        if tCnt % 100 == 0:
            print(str(tCnt)+" of "+str(len(tracks))+" tracks inserted")
        client[cfg.databaseName][cfg.trainingTracks].insert_one(
            {"track_uri":t['_id']['track_uri'],
             # "tag": tag,
             "name":t['_id']['track_name'],
             "artist_uri":t['_id']['artist_uri'],
             "artist_name":t['_id']['artist_name'],
             "album_uri":t['_id']['album_uri'],
             "count":t['count'],
             'position_metric':t['position_metric'],
             'popularity_metric':t['popularity_metric']
             }
        )

#for each challenge playlist that is not given a name directly, predict a name and store it in the challengePlaylists collection specified in the config
#prediction is done by calculating the similarity (see playlistSimilarity in util) of the current playlist and each playlist in the trainingSet collection based on their tracks where
#the first two tracks of the challenge playlist must be included in the trainingSet playlist (to limit the comparisons)
def insert_predictedName(challenge_set):
    print("predicting name for challenge playlists")
    client = pm.MongoClient(cfg.host, cfg.port)
    db = client[cfg.databaseName]
    sim = {}
    challengeTracks = []
    curTracks = []
    query = []
    name = ""
    i = 0
    for challengePlaylist in challenge_set:
        if not "name" in challengePlaylist:
            i+=1
            if i%100 == 0:
                print("predicted "+str(i)+" playlist names")
            for t in challengePlaylist['tracks']:
                challengeTracks.append(t['track_uri'])
                if len(query) <= 1:
                    query.append({"tracks.track_uri": t['track_uri']})
            res = db[cfg.trainingSet].find({"$and": query})
            for r in res:
                if len(r['tracks']) >= len(challengeTracks):
                    for t in r['tracks']:
                        curTracks.append(t['track_uri'])
                    s = util.playlistSimilarity(challengeTracks, curTracks)
                    sim[r['pid']] = s
            try:
                pid = list(sorted(sim, key=sim.get, reverse=False))[0]
                res = db[cfg.trainingSet].find({'pid': pid})
                name = res[0]['name']
                db[cfg.challengePlaylists].insert_one({'pid': challengePlaylist['pid'], 'predicted_name': name, 'predicted_from_pid': pid})
                print("inserting name")
            except:
                #print("no sim "+str(challengePlaylist['pid']))
                None