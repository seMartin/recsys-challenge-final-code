import pymongo
import util as util
import readData as read
import config as cfg

class NameClusterSelectionPredictor:
    def __init__(self, logging=False):
        self.client = pymongo.MongoClient('localhost', 27017)
        self.db = self.client[cfg.databaseName]
        self.lastPlaylist = util.buildRandomPlaylist(read.read(cfg.paths['challenge_file']), self.db)

    def predict(self, challengePlaylist, exclude_challenge_playlist = False):
        challenge_tracks = [track['track_uri'] for track in challengePlaylist['tracks']]
        predicted_playlist = []

        #select a name for the playlist, either the given one or the predicted one

        playlistName = ""
        if "name" in challengePlaylist:
            playlistName = challengePlaylist['name']
        else:
            try:
                playlistName = self.db[cfg.challengePlaylists].find({'pid':challengePlaylist['pid']})[0]['predicted_name']
            except:
                None
        tracks_name = []
        tracks_cleanName = []
        tracks_counted = []
        if playlistName != "":
            try:
                #fetch all playlists with the same name and count their distinct tracks
                tracks_name = util.trackPopularityByName(self.db, playlistName, challengePlaylist['pid'])
                tracks_counted = util.count_tracks(tracks_name, tracks_cleanName)
                #if these tracks are not enough (i. e. < num_tracks) to the same based on the cleaned name
                if len(tracks_counted) < challengePlaylist['num_tracks']:
                    tracks_cleanName = util.trackPopularityByName(self.db, util.clean_name(playlistName)[1], challengePlaylist['pid'], "clean_name")
                    tracks_counted = util.count_tracks(tracks_name, tracks_cleanName)
            except:
                None
        #if challenge tracks are given
        if len(challengePlaylist["tracks"]) > 0:
            #if name based prediction produces too few tracks, use them directly
            if len(tracks_counted) <= challengePlaylist['num_tracks']:
                for t in tracks_counted:
                    if t not in challenge_tracks:
                        #to the current prediction add each predicted track if it's not already predicted and not part of the challenge tracks
                        predicted_playlist.append(t)
                        if len(predicted_playlist) >= challengePlaylist['num_tracks']:
                            break
            else:
                #calculate the genre distribution of the challenge tracks
                genres = util.getGenreDistribution(challengePlaylist['tracks'], self.db)
                #select tracks from the name based prediction w.r.t. the genre distribution
                tracks_genre = util.predictByGenre(genres, tracks_counted, self.db, challengePlaylist['num_tracks'], challenge_tracks)
                for t in tracks_genre:
                    if t not in challenge_tracks:
                        #to the current prediction add each predicted track if it's not already predicted and not part of the challenge tracks
                        predicted_playlist.append(t)
                        if len(predicted_playlist) >= challengePlaylist['num_tracks']:
                            break
                #if genre based prediction didn't produce engough tracks, fill up with tracks from name based prediction until num_tracks is reached
                if len(predicted_playlist) < challengePlaylist['num_tracks']:
                    for t in tracks_counted:
                        if t not in challenge_tracks and t not in predicted_playlist:
                            #to the current prediction add each predicted track if it's not already predicted and not part of the challenge tracks
                            predicted_playlist.append(t)
                            if len(predicted_playlist) >= challengePlaylist['num_tracks']:
                                break
        #if no challenge tracks are given, use name based tracks directly, ordered by their number of occurrences
        else:
            for t in tracks_counted:
                if t not in challenge_tracks:
                    #to the current prediction add each predicted track if it's not already predicted and not part of the challenge tracks
                    predicted_playlist.append(t)
                    if len(predicted_playlist) >= challengePlaylist['num_tracks']:
                        break

        #if num_tracks < 500, we fill up the remaining tracks from the playlists with the most followers
        if len(predicted_playlist) < 500:
            for l in self.lastPlaylist:
                if l not in predicted_playlist and l not in challenge_tracks:
                    predicted_playlist.append(l)
                    if len(predicted_playlist) >= 500:
                        break
        else:
            print("NO NAME AND TRACKS")
        return predicted_playlist
